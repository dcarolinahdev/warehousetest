# -*-coding:utf-8-*-
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render
from .models import Person
from resourcestock.models import ResourceStock

class PersonList(ListView):
    model = Person

class PersonDetail(DetailView):
    model = Person

class PersonCreation(CreateView):
    model = Person
    success_url = reverse_lazy('personal:person_list')
    fields = '__all__'
