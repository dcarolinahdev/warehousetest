from django.contrib import admin
from .models import Person

@admin.register(Person)
class AdminPerson(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name')
    list_filter = ('id','last_name',)
