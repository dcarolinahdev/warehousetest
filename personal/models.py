# -*-coding:utf-8-*-
from __future__ import unicode_literals

from django.db import models

class Person(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    def __str__(self):
        return ('%s %s' % (self.first_name, self.last_name))

    class Meta:
        ordering = ('last_name', )
