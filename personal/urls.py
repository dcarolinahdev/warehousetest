from django.contrib.auth import views as auth_views
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^list_persons$', views.PersonList.as_view(), name='person_list'),
    url(r'^person/(?P<pk>[0-9]+)/$', views.PersonDetail.as_view(), name="person_detail"),
    url(r'^person/new', views.PersonCreation.as_view(), name="new_person"),
]
