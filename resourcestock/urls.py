from django.contrib.auth import views as auth_views
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^list_resources$', views.ResourceStockList.as_view(), name='resource_list'),
    url(r'^resource/new', views.ResourceStockCreation.as_view(), name="new_resource"),
    url(r'^resource/(?P<pk>[0-9]+)/$', views.ResourceStockDetail.as_view(), name="resource_detail"),

    url(r'^list_assignment$', views.ResourcesAssignmentList.as_view(), name='assignment_list'),
    url(r'^assignment/new', views.ResourcesAssignmentCreation.as_view(), name="new_assignment"),

    url(r'^search_assignments$', views.search_assignment, name='search_assignment'),
    url(r'^search_assignments/(?P<idPerson>[0-9]+)/$', views.search_assignment, name='search_assignment'),

    url(r'^move_assignment$', views.move_assignment, name='move_assignment'),
    url(r'^move_assignment/(?P<idAssignment>[0-9]+)/$', views.move_assignment, name='move_assignment'),
]
