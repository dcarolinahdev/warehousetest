from django.contrib import admin
from .models import ResourceStock

@admin.register(ResourceStock)
class AdminResourceStock(admin.ModelAdmin):
    list_display = ('id', 'category', 'brand', 'name', 'code')
    list_filter = ('category', 'brand', 'name',)
