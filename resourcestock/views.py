# -*-coding:utf-8-*-
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from .models import ResourceStock, ResourcesAssignment
from django.apps import apps
from .forms import SearchPersonForm, ResourcesAssignmentForm, MovePersonForm, Person
from datetime import datetime

# Index
def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render({}, request))

# Resources
class ResourceStockList(ListView):
    model = ResourceStock

class ResourceStockDetail(DetailView):
    model = ResourceStock

class ResourceStockCreation(CreateView):
    model = ResourceStock
    success_url = reverse_lazy('resourcestock:resource_list')
    fields = '__all__'

# ResourcesAssignment
class ResourcesAssignmentList(ListView):
    model = ResourcesAssignment
    queryset = ResourcesAssignment.objects.order_by('-date_assignment')

class ResourcesAssignmentCreation(CreateView):
    model = ResourcesAssignment
    form_class = ResourcesAssignmentForm
    success_url = reverse_lazy('resourcestock:assignment_list')

def search_assignment(request, idPerson=None):
    person = None
    assignment_list = []
    try:
        person = Person.objects.get(id=idPerson)
    except Exception as e:
        pass

    if request.method == 'POST':
        form = SearchPersonForm(request.POST)
        if form.is_valid():
            person = form.cleaned_data['person']
            return redirect("resourcestock:search_assignment", person.id)

    if person:
        form = SearchPersonForm({'person': person.id})
        assignment_list = ResourcesAssignment.objects.filter(person_warehouse=person, assigned=True)
    else:
        form = SearchPersonForm()

    template = loader.get_template('resourcestock/search_assignments.html')
    context = {
        'form': form,
        'person': person,
        'assignment_list': assignment_list,
    }
    return HttpResponse(template.render(context, request))

def move_assignment(request, idAssignment=None):
    assignment = None
    try:
        assignment = ResourcesAssignment.objects.get(id=idAssignment)
    except Exception as e:
        pass
    if request.method == 'POST':
        form = MovePersonForm(request.POST, id=idAssignment)
        if form.is_valid():
            # Actual Registro
            personId = assignment.person_warehouse.id
            assignment.assigned = False
            assignment.save()
            # Nuevo Registro
            fecha_hora_actual = datetime.now()
            new_person = form.cleaned_data['person']
            new_assignment = ResourcesAssignment(
                person_warehouse=new_person,
                resource_warehouse=assignment.resource_warehouse,
                date_assignment=fecha_hora_actual,
                assigned=True,
            )
            new_assignment.save()
            return redirect("resourcestock:search_assignment", personId)
    else:
        form = MovePersonForm(id=idAssignment)

    template = loader.get_template('resourcestock/move_assignment.html')
    context = {
        'form': form,
    }
    return HttpResponse(template.render(context, request))
