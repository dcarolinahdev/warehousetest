from django.apps import AppConfig


class ResourcestockConfig(AppConfig):
    name = 'resourcestock'
