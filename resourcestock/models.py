# -*-coding:utf-8-*-
from __future__ import unicode_literals

from django.db import models
from personal.models import Person

class ResourceStock(models.Model):
    category = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    brand = models.CharField(max_length=255)

    def __str__(self):
        return (self.name)

    class Meta:
        ordering = ('name', )

class ResourcesAssignment(models.Model):
    person_warehouse = models.ForeignKey(Person)
    resource_warehouse = models.ForeignKey(ResourceStock)
    date_assignment = models.DateTimeField(auto_now_add=True, blank=True)
    assigned = models.BooleanField(default=True)

    def __str__(self):
        return ('%s - %s %s' % (self.resource_warehouse.name, self.person_warehouse.first_name, self.person_warehouse.last_name))

    class Meta:
        ordering = ('date_assignment', )
