from django import forms
from .models import ResourceStock, ResourcesAssignment
from personal.models import Person

def adicionarClase(campo, clase):
    campo.widget.attrs.update({'class': clase})
    return campo

class SearchPersonForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchPersonForm, self).__init__(*args, **kwargs)
        self.fields['person'] = adicionarClase(self.fields['person'], 'one')

    person = forms.ModelChoiceField(required=True, queryset=Person.objects.all(), widget=forms.Select(attrs={'size': '6', 'readonly': True}))

class MovePersonForm(forms.Form):
    person = forms.ModelChoiceField(required=True, queryset=None)

    def __init__(self, *args, **kwargs):
        self.id_asignado = kwargs.pop('id')
        super(MovePersonForm, self).__init__(*args, **kwargs)

        old_p = ResourcesAssignment.objects.get(id=self.id_asignado).person_warehouse
        self.fields['person'].queryset = Person.objects.all().exclude(id=old_p.id)
        self.fields['person'] = adicionarClase(self.fields['person'], 'one')

class ResourcesAssignmentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResourcesAssignmentForm, self).__init__(*args, **kwargs)
        self.fields['person_warehouse'] = adicionarClase(self.fields['person_warehouse'], 'one')
        self.fields['resource_warehouse'] = adicionarClase(self.fields['resource_warehouse'], 'one')

        assignment_resources = ResourcesAssignment.objects.all()
        id_assigned_resources = []
        for i in assignment_resources:
            id_assigned_resources.append(i.resource_warehouse.id)
        visibles_resources = ResourceStock.objects.exclude(id__in=id_assigned_resources)

        self.fields['resource_warehouse'].queryset = visibles_resources

    class Meta:
        model = ResourcesAssignment
        fields = ['person_warehouse', 'resource_warehouse']
