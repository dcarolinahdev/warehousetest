Aplicación Bodega Prueba Deltec
===============================

Este Proyecto está realizado con las siguientes versiones:

Python 2.7.12
Django==1.9
(Ver + en el archivo requirements.txt)

Primer Paso:
------------

### Creación Base de Datos ###

    psql -U postgres -h localhost -W;
    create user userdel password 'userdel5';
    create database stocktaking with owner=userdel;

Segundo Paso:
-------------

###Iniciar Proyecto###

    django-admin.py startproject stocktaking
    cd stocktaking
    cd stocktaking

###Modificar settings.py###

En TEMPLATES/DIRS:

    os.path.join(BASE_DIR, 'templates'),

En DATABASES/default:

    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'stocktaking',
    'USER': 'userdel',
    'PASSWORD': 'userdel5',
    'HOST': '127.0.0.1',
    'PORT': '5432',

###Probar Ejecución Proyecto###

    cd ..
    python manage.py check
    python manage.py makemigrations
    python manage.py migrate
    python manage.py runserver

###Crear Superusuario###

    python manage.py createsuperuser
    - userstock
    - userstock5
    python manage.py runserver

###Para cada app###

    - modificar el settings/INSTALLED_APPS
    'nombreapp.apps.NombreappConfig',

    - modificar el urls.py general
    url(r'^', include('nombreapp.urls', namespace='nombreapp')),

    - agregar las plantillas html
